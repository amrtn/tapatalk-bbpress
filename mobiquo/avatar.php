<?php
		
define('TAPATALK_ROOT', 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']).'/');
define('IN_MOBIQUO', true);

if(file_exists('../bb-load.php')){
	include_once( '../bb-load.php' );
} else if(file_exists('../wp-load.php')){
	include_once( '../wp-load.php' );
}
else die('Could not find bbPress installation');
require_once( './mobiquo_common.php' );

if(!empty($_GET['user_id']))
	$user = bb_get_user($_GET['user_id']);
elseif(!empty($_GET['user_name']))
	$user = bb_get_user_by_name($_GET['user_name']);
	
if($user)
	$avatar = absolute_url(tt_get_avatar($user->ID));
				
if(!$user || empty($avatar)){	
	header( 'Content-type: image/gif' );
	echo chr(71).chr(73).chr(70).chr(56).chr(57).chr(97).chr(1).chr(0).chr(1).chr(0).chr(128).chr(0).chr(0).chr(0).chr(0).chr(0).chr(0).chr(0).chr(0).chr(33).chr(249).chr(4).chr(1).chr(0).chr(0).chr(0).chr(0).chr(44).chr(0).chr(0).chr(0).chr(0).chr(1).chr(0).chr(1).chr(0).chr(0).chr(2).chr(2).chr(68).chr(1).chr(0).chr(59);
	exit;
}

header("Location: ".$avatar);
exit;