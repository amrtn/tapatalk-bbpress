<?php

$startTime = microtime(true);

define('IN_MOBIQUO', true);
define('MOBIQUO_DEBUG', false);
define('FORUM_ROOT', 'http://'.$_SERVER['HTTP_HOST'].dirname(dirname($_SERVER['SCRIPT_NAME'])).'/');

if(file_exists('../bb-load.php')){
	include_once( '../bb-load.php' );
} else if(file_exists('../wp-load.php')){
	include_once( '../wp-load.php' ); 
}
else die('Could not find bbPress installation');

require_once './lib/xmlrpc.inc';
require_once './lib/xmlrpcs.inc';
require_once './xmlrpcs.php';

require_once './server_define.php';
require_once './mobiquo_common.php';
require_once './input.php';

$mobiquo_config = get_mobiquo_config();
$request_method_name = get_method_name();

$errorReporting = ini_get('error_reporting') &~ 8096 & ~E_NOTICE & ~E_STRICT;
@error_reporting($errorReporting);
@ini_set('error_reporting', $errorReporting);
// Hide errors from normal display - will be cleanly output via shutdown function. 
ini_set('display_errors', 0);

restore_error_handler();

function shutdown(){
	$error = error_get_last();
	if(!empty($error)){		
		switch($error['type']){
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_PARSE:
				echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".(xmlresperror("Server error occurred: '{$error['message']} (".basename($error['file']).":{$error['line']})'")->serialize('UTF-8'));				
				break;
		}
	}
}
register_shutdown_function('shutdown');

//if(bb_is_user_logged_in() && !bb_current_user_can('read'))
//{
	//tt_error("You do not have permission to access this forum");
//}

if ($request_method_name && isset($server_param[$request_method_name]))
{
	header('Mobiquo_is_login: ' . (bb_is_user_logged_in() ? 'true' : 'false'));
	if (substr($request_method_name, 0, 2) == 'm_')
		include('./include/moderation.php');
	else
		if(file_exists('./include/'.$request_method_name.'.php'))
			include('./include/'.$request_method_name.'.php');
}

$rpcServer = new Tapatalk_xmlrpcs($server_param, false);
$rpcServer->setDebug(MOBIQUO_DEBUG ? 3 : 1);
$rpcServer->compress_response = 'true';
$rpcServer->response_charset_encoding = 'UTF-8'; 

if(!empty($_POST['method_name'])){
	$xml = new xmlrpcmsg($_POST['method_name']);
	$request = $xml->serialize();
	$response = $rpcServer->service($request);	
} else {
	$response = $rpcServer->service();	
}
