<?php

defined('IN_MOBIQUO') or exit;


function get_quote_post_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
			'post_id' => Tapatalk_Input::INT,
	), $xmlrpc_params);       
	
	if(!$post = bb_get_post($input['post_id']))
		return xmlrespfalse("Post not found");	
	
	$post_text = strip_tags($post->post_text, '<strong><a><blockquote><em><img><code>');
	
	$result = new xmlrpcval(array(
		'post_id'       => new xmlrpcval($post->post_id),
		'post_title'    => new xmlrpcval('', 'base64'),
		'post_content'  => new xmlrpcval("<blockquote>{$post_text}</blockquote>", 'base64'),
	), 'struct');

	return new xmlrpcresp($result);
}
