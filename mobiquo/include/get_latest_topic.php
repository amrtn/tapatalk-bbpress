<?php

defined('IN_MOBIQUO') or exit;

function get_latest_topic_func($xmlrpc_params)
{
	global $bbdb;
		
	$input = Tapatalk_Input::filterXmlInput(array(
		'start_num' => Tapatalk_Input::INT,
		'last_num' => Tapatalk_Input::INT,
		'filters'   => Tapatalk_Input::RAW,
	), $xmlrpc_params);    
	
	if(!empty($input['filters'])){
		if(!empty($input['filters']['only_in'])){
			$input['filters']['only_in'] = array_map("intval", $input['filters']['only_in']);
			$where_sql .= " AND t.forum_id IN(".implode(',', $input['filters']['only_in']).")";        
		}
		if(!empty($input['filters']['not_in'])){
			$input['filters']['not_in'] = array_map("intval", $input['filters']['not_in']);
			$where_sql .= " AND t.forum_id NOT IN(".implode(',', $input['filters']['not_in']).")";
		}
	}		
	
	list($start, $limit, $page) = process_page($input['start_num'], $input['last_num']);
	
	$topic_list = array();    
	
	$total = $bbdb->get_var("
		SELECT COUNT(*)
		FROM $bbdb->topics AS t 
		WHERE (t.topic_status = '0') $where_sql
	");
	
	$messages = $bbdb->get_results($bbdb->prepare("
		SELECT t.*, p.*, f.*
		FROM $bbdb->topics AS t 
		left join $bbdb->posts AS p ON p.topic_id = t.topic_id
		left join $bbdb->forums AS f ON f.forum_id = t.forum_id
		WHERE (t.topic_status = '0') $where_sql
		group by t.topic_id
		ORDER BY t.topic_time DESC 
		LIMIT %d,%d", $start, $limit), ARRAY_A);
	
	if(count($messages) == 0)
		return xmlrespfalse("No threads found");

	foreach($messages as $message){										  
		
		$user = bb_get_user($message['topic_poster']);		
		$new_topic = array(
			'forum_id'          => new xmlrpcval($message['forum_id'], 'string'),
			'forum_name'        => new xmlrpcval(basic_clean($message['forum_name']), 'base64'),
			'topic_id'          => new xmlrpcval($message['topic_id'], 'string'),
			'topic_title'       => new xmlrpcval(basic_clean($message['topic_title']), 'base64'),
			'post_author_name'  => new xmlrpcval(get_user_display_name($message['topic_poster']), 'base64'),
	'last_reply_author_display_name'  => new xmlrpcval(get_user_display_name($message['topic_poster']), 'base64'),
			'icon_url'          => new xmlrpcval(absolute_url(tt_get_avatar($message['topic_poster'])), 'string'),
			'last_reply_time'   => new xmlrpcval(mobiquo_iso8601_encode(strtotime($message['topic_time'])), 'dateTime.iso8601'),
			'reply_number'      => new xmlrpcval($message['topic_posts'] - 1, 'int'),
			'view_number'       => new xmlrpcval(0, 'int'),
			'new_post'          => new xmlrpcval(false, 'boolean'),
			'short_content'     => new xmlrpcval(process_short_content($message['post_text']), 'base64'),
			'can_subscribe'     => new xmlrpcval(bb_current_user_can('edit_favorites'), 'boolean'), 
			'is_subscribed'     => new xmlrpcval(is_user_favorite(bb_get_current_user()->ID, $message['topic_id']), 'boolean'),
			'is_closed'         => new xmlrpcval(!$message['topic_open'], 'boolean'),
			
			'can_delete'        => new xmlrpcval(bb_current_user_can('delete_topic', $message['topic_id']), 'boolean'),
			'can_close'         => new xmlrpcval(bb_current_user_can('close_topic', $message['topic_id']), 'boolean'),
			'can_approve'       => new xmlrpcval(false, 'boolean'),
			'can_stick'         => new xmlrpcval(bb_current_user_can('stick_topic', $message['topic_id']), 'boolean'),
			'can_move'          => new xmlrpcval(bb_current_user_can('move_topic', $message['topic_id']), 'boolean'),
			'can_ban'           => new xmlrpcval(bb_current_user_can('edit_user', $message['topic_poster']) && $message['topic_poster'] != bb_get_current_user()->ID  && bb_current_user_can('moderate'), 'boolean'),
			'can_rename'        => new xmlrpcval(bb_current_user_can('edit_topic', $message['topic_id']), 'boolean'), 
			'is_ban'            => new xmlrpcval(array_key_exists('blocked', $user->capabilities), 'boolean'),
			'is_sticky'         => new xmlrpcval($message['topic_sticky'], 'boolean'),
			'is_approved'       => new xmlrpcval(true, 'boolean'),
			'is_deleted'        => new xmlrpcval($message['topic_status'] != 0, 'boolean')
				
		);
	
			
		if($_SERVER['HTTP_MOBIQUO_ID'] == 10)
		{
			$avatars = $bbdb->get_results($bbdb->prepare("
				select poster_id 
				from $bbdb->posts
				where topic_id = %d
				group by poster_id
				order by count(poster_id) desc
				limit 10
			", $message->topic_id));
						
			$icon_urls_list = array();   
			foreach($avatars as $avatar_user_id){
				if(empty($avatar_user_id))
					continue;
				$icon_urls_list []= new xmlrpcval($avatar_user_id, "string");
			}      
			$new_topic['participated_uids'] = new xmlrpcval($icon_urls_list, 'array');       
		}
		
		$topic_list[] = new xmlrpcval($new_topic, 'struct');
		
	}                

	$result = new xmlrpcval(array(
		'total_topic_num' => new xmlrpcval($total, 'int'),
		'result'          => new xmlrpcval(true, 'boolean'),
		'result_text'     => new xmlrpcval('', 'base64'),
		'topics'          => new xmlrpcval($topic_list, 'array'),
	), 'struct');

	return new xmlrpcresp($result);
}
