<?php
	
defined('IN_MOBIQUO') or exit;

function get_id_by_url_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'url' => Tapatalk_Input::STRING,
	), $xmlrpc_params);
	
	$url = trim($input['url']);
	
	$page = bb_get_path(0, null, $url);    
	$id = bb_get_path(1, null, $url);
	
	if(empty($id)){
		$page = strpos($url, 'topic.php') !== false ? 'topic' : 'forum';
		$urlParsed = parse_url($url);
		parse_str($urlParsed['query'], $urlQuery);
		if(!empty($urlQuery['id']))
			$id = $urlQuery['id'];
		else $id = $urlQuery[$page];
	}
	
	$fid = $tid = $pid = "";
	
	if($page == 'topic'){
		$topic = get_topic($id);
		$tid = $topic->topic_id;
	} else if($page == 'forum'){
		$forum = get_forum($id);
		$fid = $forum->forum_id;	
	}    	
	
	// get post id
	if (preg_match('/(?:\?|&|;|\/|#)(?:p|pid|post)(?:\.php)?(?:=|\/|\?id=|-)(\d+)/', $url, $match)) {
		$pid = $match[1];
	}
		
	
	$result = array();
	if ($fid) $result['forum_id'] = new xmlrpcval($fid, 'string');
	if ($tid) $result['topic_id'] = new xmlrpcval($tid, 'string');
	if ($pid) $result['post_id'] = new xmlrpcval($pid, 'string');
	
	$response = new xmlrpcval($result, 'struct');
	
	return new xmlrpcresp($response);
}
