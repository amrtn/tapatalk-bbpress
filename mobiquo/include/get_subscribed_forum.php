<?php

defined('IN_MOBIQUO') or exit;


function get_subscribed_forum_func($xmlrpc_params)
{	
	$result = new xmlrpcval(array(
		'total_forums_num' => new xmlrpcval(0, 'int'),
		'forums'           => new xmlrpcval(array(), 'array')
	), 'struct');

	return new xmlrpcresp($result);
}
