<?php

defined('IN_MOBIQUO') or exit;

function new_topic_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'forum_id' => Tapatalk_Input::INT,
		'subject' => Tapatalk_Input::STRING,
		'message' => Tapatalk_Input::STRING,
		'prefix_id' => Tapatalk_Input::STRING,
		'attachment_id_array' => Tapatalk_Input::RAW,
		'group_id' => Tapatalk_Input::INT,
	), $xmlrpc_params);
	
	if(!bb_current_user_can('write_topic') || !bb_current_user_can('write_post'))
		return xmlrespfalse("You do not have permission to post new topics");
	
	if(!$forum = get_forum($input['forum_id']))
		return xmlrespfalse("Invalid forum specified");
	
	if(!bb_current_user_can('write_topic', $forum->forum_id))
		return xmlrespfalse("You do not have permission to post new topics in this forum");
		
	if(empty($input['subject']))
		return xmlrespfalse("You must enter a subject");
	
	if(empty($input['message']))
		return xmlrespfalse("You must enter a message");

	if(!$topic_id = bb_insert_topic(array(
		'topic_title' => $input['subject'],
		'forum_id' => $forum->forum_id
	)))
		return xmlrespfalse("The topic could not be created");
	
	
	if(!bb_insert_post(array(
		'topic_id' => $topic_id,
		'post_text' => $input['message']
	)))
		return xmlrespfalse("The post could not be created");

	$topic = get_topic($topic_id);
		
			
	$result = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval('', 'base64'),
		'topic_id'      => new xmlrpcval($topic_id, 'string'),
		'state'         => new xmlrpcval($topic->topic_status == 0 ? 0 : 1, 'int'),
	), 'struct');
		
	return new xmlrpcresp($result);
}