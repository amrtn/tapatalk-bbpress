<?php

defined('IN_MOBIQUO') or exit;

require_once "include/get_thread.php";

function get_thread_by_post_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'post_id'    => Tapatalk_Input::INT,
		'posts_per_request'   => Tapatalk_Input::INT,
		'return_html' => Tapatalk_Input::INT
	), $xmlrpc_params);
	
	if(!$input['posts_per_request'])
		$input['posts_per_request'] = 20;
	
	if(!$post = bb_get_post($input['post_id']))
		return xmlrespfalse("Post not found");
						
	$page = floor($post->post_position / $input['posts_per_request']) + 1;
	
	$response = get_thread_func(new xmlrpcval(array(
		new xmlrpcval($post->topic_id, "string"),
		new xmlrpcval(($page-1) * $input['posts_per_request'], 'int'),
		new xmlrpcval(($page-1) * $input['posts_per_request'] + $input['posts_per_request'] - 1, 'int'),
		new xmlrpcval(!!$input['return_html'], 'boolean'),
	), 'array'));
	
	$response->val->addStruct(array(
		'position' => new xmlrpcval($post->post_position, 'int'),
	));
	
	return $response;
}
