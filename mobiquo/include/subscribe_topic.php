<?php

defined('IN_MOBIQUO') or exit;

function subscribe_topic_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id' => Tapatalk_Input::INT,
		'notification_mode' => Tapatalk_Input::INT,
	), $xmlrpc_params);     
	
	$user_id = bb_get_current_user()->ID;
	if(!bb_current_user_can('edit_favorites_of', $user_id))
		return xmlrespfalse("You do not have permission to edit favorite topics");
		
	bb_add_user_favorite($user_id, $input['topic_id']);
	
	return xmlresptrue();
}
