<?php

defined('IN_MOBIQUO') or exit;


function get_forum_func()
{	
	global $bb_forums_loop, $bbdb;	
	
	$forums = bb_get_forums();
	$xml_nodes = new xmlrpcval(array(), 'array');
	$done=array();
	$xml_tree = treeBuild(0, $forums, $xml_nodes, $done);
	$xml_nodes->addArray($xml_tree);

	return new xmlrpcresp($xml_nodes);
}



function processForum($forum){			
	$xmlrpc_forum = new xmlrpcval(array(
		'forum_id'      => new xmlrpcval($forum->forum_id, 'string'),
		'forum_name'    => new xmlrpcval(basic_clean($forum->forum_name), 'base64'),
		'description'   => new xmlrpcval(basic_clean($forum->forum_desc), 'base64'),
		'parent_id'     => new xmlrpcval($forum->forum_parent, 'string'),
		//'logo_url'      => new xmlrpcval($icon, 'string'),
		'new_post'      => new xmlrpcval(false, 'boolean'),
		'is_protected'  => new xmlrpcval(false, 'boolean'),
		'url'           => new xmlrpcval("", 'string'),
		'sub_only'      => new xmlrpcval((bool)@$forum->forum_is_category, 'boolean'),
		'can_subscribe' => new xmlrpcval(false, 'boolean'),
		'is_subscribed' => new xmlrpcval(false, 'boolean'),
	), 'struct');
	
	return $xmlrpc_forum;
}


function treeBuild($parent_id, &$cats, &$xml_nodes, &$done){    
	$newCats = array();
	foreach($cats as $id => $cat){
		
		if($cat->forum_parent == $parent_id && !array_key_exists($id, $done)){
			$cat2 = processForum($cat);    
			$done[$id] = true;
			$child_cats = treeBuild($id, $cats, $xml_nodes, $done);
			/*if (empty($child_cats))
			{
				if ($cat->forum_parent == 0) continue;
			}
			else*/
			if(!empty($child_cats))
				$cat2->addStruct(array('child' => new xmlrpcval($child_cats, 'array')));

			$newCats[] = $cat2;
			
		}
	}
	
	return $newCats;    
}
