<?php

defined('IN_MOBIQUO') or exit;


function get_topic_status_func($xmlrpc_params)
{    
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id_array' => Tapatalk_Input::RAW
	), $xmlrpc_params);
	
	$topic_list = array();
	foreach($input['topic_id_array'] as $topic_id){
		
		$message = get_topic((int)$topic_id);
		if(!$message) 
			continue;
	
		$new_topic = array(
			'topic_id'          => new xmlrpcval($message->topic_id, 'string'),
			'is_subscribed'     => new xmlrpcval(is_user_favorite(bb_get_current_user()->ID, $message->topic_id), 'boolean'),
			'can_subscribe'     => new xmlrpcval(bb_current_user_can('edit_favorites'), 'boolean'),    
			'is_closed'         => new xmlrpcval(!$message->topic_open, 'boolean'),
			'last_reply_time'   => new xmlrpcval(mobiquo_iso8601_encode(strtotime($message->topic_time)), 'dateTime.iso8601'),
			'new_post'          => new xmlrpcval(false, 'boolean'),
			'reply_number'      => new xmlrpcval($message->topic_posts - 1, 'int'),
			'view_number'       => new xmlrpcval(0, 'int'),				
		);
		
		$topic_list[] = new xmlrpcval($new_topic, 'struct');

	}

	$result = new xmlrpcval(array(
		'result'          => new xmlrpcval(true, 'boolean'),
		'result_text'     => new xmlrpcval('', 'base64'),
		'status'          => new xmlrpcval($topic_list, 'array'),
	), 'struct');

	return new xmlrpcresp($result);
}
