<?php

defined('IN_MOBIQUO') or exit;

function addCustomField($name, $value, &$list){
	$list[] = new xmlrpcval(array(
		'name'  => new xmlrpcval($name, 'base64'),
		'value' => new xmlrpcval($value, 'base64')
	), 'struct');
}
	
function get_user_info_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'user_name' => Tapatalk_Input::RAW,
		'user_id' => Tapatalk_Input::INT,
	), $xmlrpc_params);
	
	if(!empty($input['user_id']))
		$user = bb_get_user($input['user_id']);
	elseif(!empty($input['user_name']))
		$user = bb_get_user_by_name($input['user_name']);
	else
		$user = bb_get_current_user();
					
	if(!$user)
		return xmlrespfalse("User not found");
		
	$posts = (int)$bbdb->get_var($bbdb->prepare("SELECT COUNT(*) FROM $bbdb->posts WHERE poster_id = %d", $user->ID));
	$last_post = (int)$bbdb->get_var($bbdb->prepare("SELECT UNIX_TIMESTAMP(MAX(post_time)) FROM $bbdb->posts WHERE poster_id = %d", $user->ID));

	$custom_fields_list = array();
	
	if(!empty($user->occ))
		$custom_fields_list[]=new xmlrpcval(array(
			'name' => new xmlrpcval('Occupation', 'base64'),
			'value' => new xmlrpcval($user->occ, 'base64')
		), 'struct');
	if(!empty($user->from))
		$custom_fields_list[]=new xmlrpcval(array(
			'name' => new xmlrpcval('Location', 'base64'),
			'value' => new xmlrpcval($user->from, 'base64')
		), 'struct');	
	if(!empty($user->interest))
		$custom_fields_list[]=new xmlrpcval(array(
			'name' => new xmlrpcval('Interests', 'base64'),
			'value' => new xmlrpcval($user->interest, 'base64')
		), 'struct');
	if(!empty($user->user_url))
		$custom_fields_list[]=new xmlrpcval(array(
			'name' => new xmlrpcval('Website', 'base64'),
			'value' => new xmlrpcval($user->user_url, 'base64')
		), 'struct');
		
	if(!empty($user->aim))
		$custom_fields_list[]=new xmlrpcval(array(
			'name' => new xmlrpcval('AIM', 'base64'),
			'value' => new xmlrpcval($user->aim, 'base64')
		), 'struct');
	if(!empty($user->jabber))
		$custom_fields_list[]=new xmlrpcval(array(
			'name' => new xmlrpcval('Jabber', 'base64'),
			'value' => new xmlrpcval($user->jabber, 'base64')
		), 'struct');
	if(!empty($user->yim))
		$custom_fields_list[]=new xmlrpcval(array(
			'name' => new xmlrpcval('Yahoo IM', 'base64'),
			'value' => new xmlrpcval($user->yim, 'base64')
		), 'struct');
	if(!empty($user->description))
		$custom_fields_list[]=new xmlrpcval(array(
			'name' => new xmlrpcval('About', 'base64'),
			'value' => new xmlrpcval($user->description, 'base64')
		), 'struct');
		
	$xmlrpc_user_info = new xmlrpcval(array(
		'post_count'         => new xmlrpcval($posts, 'int'),
		'reg_time'           => new xmlrpcval(mobiquo_iso8601_encode(strtotime($user->user_registered)), 'dateTime.iso8601'),
		'user_name'          => new xmlrpcval($user->user_login, 'base64'),
		'user_id'            => new xmlrpcval($user->ID, 'string'),
		'display_name'       => new xmlrpcval(get_user_display_name($user->ID), 'base64'),
		'last_activity_time' => new xmlrpcval(mobiquo_iso8601_encode($last_post), 'dateTime.iso8601'),
		'is_online'          => new xmlrpcval(false, 'boolean'),
		'accept_pm'          => new xmlrpcval(false, 'boolean'),
		'i_follow_u'         => new xmlrpcval(false, 'boolean'),
		'u_follow_me'        => new xmlrpcval(false, 'boolean'),
		'accept_follow'      => new xmlrpcval(false, 'boolean'),
		'following_count'    => new xmlrpcval(0, 'int'), 
		'follower'           => new xmlrpcval(0, 'int'), 
		'display_text'       => new xmlrpcval(@$user->bb_title, 'base64'),
		'icon_url'           => new xmlrpcval(absolute_url(tt_get_avatar($user->ID)), 'string'),
		'current_activity'   => new xmlrpcval('', 'base64'), 
		'current_action'     => new xmlrpcval('', 'base64'), 
		'custom_fields_list' => new xmlrpcval($custom_fields_list, 'array'),
		
		'can_ban'            => new xmlrpcval(bb_current_user_can('edit_users') && $user->ID != bb_get_current_user()->ID, 'boolean'),
		'is_ban'             => new xmlrpcval(array_key_exists('blocked', $user->capabilities), 'boolean'),
	), 'struct');

	return new xmlrpcresp($xmlrpc_user_info);
	
	
}
