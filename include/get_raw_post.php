<?php

defined('IN_MOBIQUO') or exit;


function get_raw_post_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
			'post_id' => Tapatalk_Input::INT,
	), $xmlrpc_params);       
	
	if(!$post = bb_get_post($input['post_id']))
		return xmlrespfalse("Post not found");    
	
	if(!$topic = get_topic($post->topic_id))
		return xmlrespfalse("Topic not found");
	
	if(!bb_current_user_can('edit_post', $post->post_id))
		return xmlrespfalse("You do not have permission to edit this post");
		/*
	if(($message->post_status != 0 || $topic->topic_status != 0) && !bb_current_user_can('edit_deleted'))
		return xmlrespfalse("You do not have permission to edit deleted posts");
		
	if($topic->topic_open == 0 && !bb_current_user_can('edit_closed'))
		return xmlrespfalse("You do not have permission to edit posts in closed topics");*/
			
	$post_text = strip_tags($post->post_text, '<strong><a><blockquote><em><img><code>');
	//$post_text = str_replace("<br />", "");
			
	$result = new xmlrpcval(array(
		'post_id'       => new xmlrpcval($post->post_id),
		'post_title'    => new xmlrpcval(( bb_is_first($post->post_id) && bb_current_user_can('edit_topic', $topic->topic_id)) ? $topic->topic_title : "", 'base64'),
		'post_content'  => new xmlrpcval($post_text, 'base64'),
	), 'struct');

	return new xmlrpcresp($result);
}
