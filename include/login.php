<?php

defined('IN_MOBIQUO') or exit;


function login_response($user)
{
	bb_set_current_user($user->ID);
	
	$groups = array();
	foreach(bb_get_current_user()->roles as $role){
		$groups[]=new xmlrpcval($role, "string");
	}
	
	$result = new xmlrpcval(array(
		'result'            => new xmlrpcval(true, 'boolean'),
		'result_text'       => new xmlrpcval('', 'base64'),
		'usergroup_id'      => new xmlrpcval($groups, "array"),
		'can_pm'            => new xmlrpcval(false, "boolean"),
		'can_send_pm'       => new xmlrpcval(false, "boolean"),
		'can_moderate'      => new xmlrpcval(bb_current_user_can('moderate'), "boolean"),
		'can_upload_avatar' => new xmlrpcval(false, "boolean"),
		'can_report_pm'     => new xmlrpcval(false, 'boolean'),
		'max_attachment'    => new xmlrpcval(0, "int"),
		'max_png_size'      => new xmlrpcval(0, "int"),
		'max_jpg_size'      => new xmlrpcval(0, "int"),
	), 'struct');

	return new xmlrpcresp($result);    
	
}

function login_func($xmlrpc_params)
{	
	global $mobiquo_config;
			
	$input = Tapatalk_Input::filterXmlInput(array(
		'username' => Tapatalk_Input::RAW,
		'password' => Tapatalk_Input::RAW,
	), $xmlrpc_params);
	
	
	if(!bb_is_user_logged_in()){
						
				
		// Do we allow login by email address
		$email_login = bb_get_option( 'email_login' );

		// Attempt to log the user in
		if ( $user = bb_login( $input['username'], $input['password'], 1 ) ) {
			if ( !is_wp_error( $user ) ) {
				
				return login_response($user);
				
			} else {
				$bb_login_error =& $user;
			}
			
		// No login so prepare the error
		} else {
			$bb_login_error = new WP_Error;
		}

		/** Handle errors *************************************************************/

		// Get error data so we can provide feedback
		$error_data = $bb_login_error->get_error_data();

		// Does user actually exist
		if ( isset( $error_data['unique'] ) && false === $error_data['unique'] )
			$user_exists = true;
		else
			$user_exists = !empty( $input['username'] ) && (bool) bb_get_user( $input['username'], array( 'by' => 'login' ) );

			
			// If the user doesn't exist then add that error
			if ( empty( $user_exists ) ) {
				if ( !empty( $input['username'] ) ) {
					$bb_login_error->add( 'user_login', __( 'User does not exist.' ) );
				} else {
					$bb_login_error->add( 'user_login', $email_login ? __( 'Enter a username or email address.' ) : __( 'Enter a username.' ) );
				}
			}

			// If the password was wrong then add that error
			if ( !$bb_login_error->get_error_code() ) {
				$bb_login_error->add( 'password', __( 'Incorrect password.' ) );
			}

		if ( !empty( $email_login ) && $bb_login_error->get_error_codes() && false !== is_email( $input['username'] ) )
			$bb_login_error = new WP_Error( 'user_login', __( 'Username and Password do not match.' ) );
			
		if($bb_login_error->get_error_code() != '')
			return xmlrespfalse("Error(s) occurred: ".implode(" :: ", $bb_login_error->get_error_messages()));


	}

	return login_response(bb_get_current_user());

}

