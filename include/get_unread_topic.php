<?php

defined('IN_MOBIQUO') or exit;

require_once "include/get_latest_topic.php";

function get_unread_topic_func($xmlrpc_params)
{	
	return get_latest_topic_func($xmlrpc_params);    
}
