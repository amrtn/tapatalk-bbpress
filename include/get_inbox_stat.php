<?php

defined('IN_MOBIQUO') or exit;

function get_inbox_stat_func($xmlrpc_params)
{	
	$input = Tapatalk_Input::filterXmlInput(array(
		'pm_last_checked_time' => Tapatalk_Input::STRING,
		'subscribed_topic_last_checked_time' => Tapatalk_Input::STRING,
	), $xmlrpc_params);
	
	$result = new xmlrpcval(array(
		'inbox_unread_count' => new xmlrpcval(0, 'int'),
		'subscribed_topic_unread_count' => new xmlrpcval(0, 'int'),
	), 'struct');

	return new xmlrpcresp($result);
}
