<?php

defined('IN_MOBIQUO') or exit;

function get_thread_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id'    => Tapatalk_Input::INT,
		'start_num'   => Tapatalk_Input::INT,
		'last_num'    => Tapatalk_Input::INT,
		'return_html' => Tapatalk_Input::INT
	), $xmlrpc_params);	
	
	list($start, $limit, $page) = process_page($input['start_num'], $input['last_num']);
	
	if(!$topic = get_topic($input['topic_id']))
		return xmlrespfalse("Invalid topic");
		
	if(!$forum = get_forum($topic->forum_id))
		return xmlrespfalse("Invalid topic");

	if(!$messages = get_thread($topic->topic_id, array('page' => $page, 'per_page' => $limit))) 
		return xmlrespfalse("No posts found in topic");

		
	if($topic->topic_status != 0 && !bb_current_user_can('browse_deleted'))
		return xmlrespfalse("You do not have permission to view deleted topics");

	$post_list = array();
	foreach($messages as $message){
		
		$user = bb_get_user($message->poster_id);    
			
		$post_list[] = new xmlrpcval(array(
			'post_id'          => new xmlrpcval($message->post_id, 'string'),
			'post_title'       => new xmlrpcval('', 'base64'), 
			'post_content'     => new xmlrpcval(process_post($message->post_text, $input['return_html']), 'base64'),
			'post_author_name' => new xmlrpcval(get_user_display_name($message->poster_id), 'base64'),
	'post_author_display_name' => new xmlrpcval(get_user_display_name($message->poster_id), 'base64'),
			'is_online'        => new xmlrpcval(false, 'boolean'),
			'can_edit'         => new xmlrpcval(bb_current_user_can('edit_post', $message->post_id), 'boolean'),
			'icon_url'         => new xmlrpcval(absolute_url(tt_get_avatar($message->poster_id)), 'string'),
			'post_time'        => new xmlrpcval(mobiquo_iso8601_encode(strtotime($message->post_time)), 'dateTime.iso8601'),
			'attachments'      => new xmlrpcval(array(), 'array'),
			'allow_smilies'    => new xmlrpcval(false, 'boolean'), 
			
			'can_delete'        => new xmlrpcval(bb_current_user_can('delete_post', $message->post_id), 'boolean'),
			'can_approve'       => new xmlrpcval(false, 'boolean'),               
			'can_move'          => new xmlrpcval(bb_current_user_can('move_topic', $message->topic_id), 'boolean'),
			'can_ban'           => new xmlrpcval(bb_current_user_can('edit_user', $message->poster_id) && $message->poster_id != bb_get_current_user()->ID && bb_current_user_can('moderate'), 'boolean'),
			'is_ban'            => new xmlrpcval(array_key_exists('blocked', $user->capabilities), 'boolean'),
			'is_approved'      => new xmlrpcval(true, 'boolean'),
			'is_deleted'       => new xmlrpcval($post->post_status != 0, 'boolean'),
		
			'can_thank'        => new xmlrpcval(false, 'boolean'),
		), 'struct');
		
	}

	
	$user = bb_get_user($topic->topic_poster); 
           
	$result = new xmlrpcval(array(
		'total_post_num'    => new xmlrpcval($topic->topic_posts, 'int'),
		'forum_id'          => new xmlrpcval($topic->forum_id, 'string'),
		'forum_name'        => new xmlrpcval(basic_clean($forum->forum_name), 'base64'),
		'topic_id'          => new xmlrpcval($topic->topic_id, 'string'),
		'topic_title'       => new xmlrpcval(basic_clean($topic->topic_title), 'base64'),
		'can_subscribe'     => new xmlrpcval(bb_current_user_can('edit_favorites'), 'boolean'),             
		'is_subscribed'     => new xmlrpcval(is_user_favorite(bb_get_current_user()->ID, $message->topic_id), 'boolean'),
		'is_closed'         => new xmlrpcval(!$topic->topic_open, 'boolean'),			
		'can_reply'         => new xmlrpcval(((bb_current_user_can('write_post', $topic->topic_id)) && ($topic->topic_open)) ? true : false, 'boolean'),		
		
		'can_delete'        => new xmlrpcval(bb_current_user_can('delete_topic', $topic->topic_id), 'boolean'),
		'can_close'         => new xmlrpcval(bb_current_user_can('close_topic', $topic->topic_id), 'boolean'),
		'can_approve'       => new xmlrpcval(false, 'boolean'),
		'can_stick'         => new xmlrpcval(bb_current_user_can('stick_topic', $topic->topic_id), 'boolean'),
		'can_move'          => new xmlrpcval(bb_current_user_can('move_topic', $topic->topic_id), 'boolean'),
		'can_ban'           => new xmlrpcval((bb_current_user_can('edit_user', $topic->topic_poster) && ($topic->topic_poster != bb_get_current_user()->ID)) && bb_current_user_can('moderate'), 'boolean'),

		'can_rename'        => new xmlrpcval(bb_current_user_can('edit_topic', $topic->topic_id), 'boolean'),
		'is_ban'            => new xmlrpcval(array_key_exists('blocked', $user->capabilities), 'boolean'),         
		'is_sticky'         => new xmlrpcval($topic->topic_sticky, 'boolean'),
		'is_approved'       => new xmlrpcval(true, 'boolean'),
		'is_deleted'        => new xmlrpcval($topic->topic_status != 0, 'boolean'),
		'can_upload'        => new xmlrpcval(false, 'boolean'),
		
		'posts'             => new xmlrpcval($post_list, 'array'),		                
	), 'struct');

	return new xmlrpcresp($result);
}
