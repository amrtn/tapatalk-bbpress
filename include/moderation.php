<?php

defined('IN_MOBIQUO') or exit;

function m_login_mod_func($xmlrpc_params){
	return xmlrespfalse("Moderator login not supported");
}

function m_stick_topic_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id'  => Tapatalk_Input::INT,
		'mode'      => Tapatalk_Input::INT,
	), $xmlrpc_params);
	
	if(!$topic = get_topic($input['topic_id']))
		return xmlrespfalse("Invalid topic");
	
	if(!bb_current_user_can('stick_topic', $topic->topic_id))
		return xmlrespfalse("You do not have permission to perform this action");

	if($input['mode'] == 1)
		bb_stick_topic($topic->topic_id);
	else
		bb_unstick_topic($topic->topic_id);
				
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);
}

function m_close_topic_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id'  => Tapatalk_Input::INT,
		'mode'      => Tapatalk_Input::INT,
	), $xmlrpc_params);	
	
	if(!$topic = get_topic($input['topic_id']))
		return xmlrespfalse("Invalid topic");
	
	if(!bb_current_user_can('close_topic', $topic->topic_id))
		return xmlrespfalse("You do not have permission to perform this action");

	if($input['mode'] == 2)
		bb_close_topic($topic->topic_id);
	else
		bb_open_topic($topic->topic_id);
				
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);
}

function m_delete_topic_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id'    => Tapatalk_Input::INT,
		'mode'        => Tapatalk_Input::INT,
		'reason_text' => Tapatalk_Input::STRING,
	), $xmlrpc_params);
		
	if(!$topic = get_topic($input['topic_id']))
		return xmlrespfalse("Invalid topic");
	
	if(!bb_current_user_can('delete_topic', $topic->topic_id))
		return xmlrespfalse("You do not have permission to perform this action");

	if(!bb_delete_topic($topic->topic_id, 1))
		return xmlrespfalse("Failed to delete topic");
	
	if($input['mode'] == 2){
		$bbdb->query($bbdb->prepare("DELETE FROM $bbdb->topics WHERE topic_id = %d", $topic->topic_id));
		$bbdb->query($bbdb->prepare("DELETE FROM $bbdb->posts WHERE topic_id = %d", $topic->topic_id));
	}
				
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);
}

function m_undelete_topic_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id'    => Tapatalk_Input::INT,
		'reason_text' => Tapatalk_Input::STRING,
	), $xmlrpc_params);
		
	if(!$topic = get_topic($input['topic_id']))
		return xmlrespfalse("Invalid topic");
	
	if(!bb_current_user_can('delete_topic', $topic->topic_id))
		return xmlrespfalse("You do not have permission to perform this action");
		
	if(!bb_delete_topic($topic->topic_id, 0))
		return xmlrespfalse("Failed to undelete topic");
					
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);    
}

function m_get_report_post_func($xmlrpc_params){		
	$result = new xmlrpcval(array(
		'total_report_num' => new xmlrpcval(0, 'int'),
		'reports'          => new xmlrpcval(array(), 'array')
	), 'struct');

	return new xmlrpcresp($result);
}

function m_move_topic_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id'    => Tapatalk_Input::INT,
		'forum_id'    => Tapatalk_Input::INT,
	), $xmlrpc_params);
	
	if(!$topic = get_topic($input['topic_id']))
		return xmlrespfalse("Invalid topic");
	
	if(!$forum = get_forum($input['forum_id']))
		return xmlrespfalse("Invalid forum");
		
	if(!bb_current_user_can('move_topic', $topic->topic_id, $forum->forum_id))
		return xmlrespfalse("You do not have permission to perform this action");
		
	if(!bb_move_topic($topic->topic_id, $forum->forum_id))
		return xmlrespfalse("Failed to move topic");    
			
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);    
}

function m_merge_topic_func($xmlrpc_params)
{
	global $bbdb;
		
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id_a'    => Tapatalk_Input::INT,
		'topic_id_b'    => Tapatalk_Input::INT,
	), $xmlrpc_params);
	
	if(!$topic_a = get_topic($input['topic_id_a']))
		return xmlrespfalse("Invalid topic");
	if(!$topic_b = get_topic($input['topic_id_b']))
		return xmlrespfalse("Invalid topic");
	
	if(!bb_current_user_can('move_topic', $topic_a->topic_id) || !bb_current_user_can('move_topic', $topic_b->topic_id))
		return xmlrespfalse("You do not have permission to perform this action");
	
	if(!$bbdb->query($bbdb->prepare("UPDATE $bbdb->posts SET topic_id = %d WHERE topic_id = %d", $topic_b->topic_id, $topic_a->topic_id)))
		return xmlrespfalse("Merging failed");
	if(!$bbdb->query($bbdb->prepare("DELETE FROM $bbdb->topics WHERE topic_id = %d", $topic_b->topic_id, $topic_a->topic_id)))
		return xmlrespfalse("Merging failed");
	
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);    
}

function m_approve_topic_func($xmlrpc_params)
{
	return xmlrespfalse("Not supported");     
}


function m_rename_topic_func($xmlrpc_params)
{
	return xmlrespfalse("Please edit the first post subject to change the thread title");
}
	
// POST ACTION

function m_delete_post_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'post_id'    => Tapatalk_Input::INT,
		'mode'        => Tapatalk_Input::INT,
		'reason_text' => Tapatalk_Input::STRING,
	), $xmlrpc_params);
	
	if(!$post = bb_get_post($input['post_id']))
		return xmlrespfalse("Invalid topic");
	
	if(!bb_current_user_can('delete_post', $post->post_id))
		return xmlrespfalse("You do not have permission to perform this action");
		
	if(!bb_delete_post($post->post_id, 1))
		return xmlrespfalse("Failed to delete post");
	
	if($input['mode'] == 2){
		$bbdb->query($bbdb->prepare("DELETE FROM $bbdb->posts WHERE post_id = %d", $post->post_id));
	}

	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);
}

function m_undelete_post_func($xmlrpc_params)
{				
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'post_id'    => Tapatalk_Input::INT,
		'reason_text' => Tapatalk_Input::STRING,
	), $xmlrpc_params);
	
	
	if(!$post = bb_get_post($input['post_id']))
		return xmlrespfalse("Invalid topic");
	
	if(!bb_current_user_can('delete_post', $post->post_id))
		return xmlrespfalse("You do not have permission to perform this action");
		
	if(!bb_delete_post($post->post_id, 0))
		return xmlrespfalse("Failed to undelete post");
							
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);		
}

function m_move_post_func($xmlrpc_params)
{    
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'post_id'    => Tapatalk_Input::INT,
		'topic_id'  => Tapatalk_Input::INT,
		'topic_title'  => Tapatalk_Input::STRING,
		'forum_id'   => Tapatalk_Input::INT,
	), $xmlrpc_params);
	
	if(!$post = bb_get_post($input['post_id']))
		return xmlrespfalse("Invalid topic");
	
	$forum = bb_get_forum($input['forum_id']);
	$topic = get_topic($input['topic_id']);
	
	if(!bb_current_user_can('move_topic'))
		return xmlrespfalse("You do not have permission to perform this action");
		
	if($topic){
		if(!$bbdb->query($bbdb->prepare("UPDATE $bbdb->posts SET topic_id = %d WHERE post_id = %d", $topic->topic_id, $post->post_id)))
			return xmlrespfalse("Failed to move post");	
		
		$bbdb->query( $bbdb->prepare(
			"UPDATE $bbdb->forums SET topics = topics + 1, posts = posts + 1 WHERE forum_id = %d", $topic->forum_id
		) );
		$bbdb->query( $bbdb->prepare( 
			"UPDATE $bbdb->forums SET topics = topics - 1, posts = posts - 1 WHERE forum_id = %d", $post->forum_id
		) );	
		
		bb_topic_set_last_post($topic->topic_id);
		
	} else {		
		
		if(!$forum)
			return xmlrespfalse("Invalid forum");
		if(empty($input['topic_title']))
			return xmlrespfalse("You must enter a topic subject");
		
		if(!$topic_id = bb_insert_topic(array(
			'topic_title' => $input['topic_title'],
			'forum_id' => $forum->forum_id
		)))
			return xmlrespfalse("The topic could not be created");
			
		$newTopic = get_topic($topic_id);
		
		if(!$bbdb->query($bbdb->prepare("UPDATE $bbdb->posts SET topic_id = %d WHERE post_id = %d", $newTopic->topic_id, $post->post_id)))
			return xmlrespfalse("Failed to move post");    
		
		$bbdb->query( $bbdb->prepare(
			"UPDATE $bbdb->forums SET topics = topics + 1, posts = posts + 1 WHERE forum_id = %d", $newTopic->forum_id
		) );
		$bbdb->query( $bbdb->prepare( 
			"UPDATE $bbdb->forums SET topics = topics - 1, posts = posts - 1 WHERE forum_id = %d", $post->forum_id
		) );    		
		
		bb_topic_set_last_post($newTopic->topic_id);
	}
	
	bb_topic_set_last_post($post->topic_id);
	
	wp_cache_flush( 'bb_post' );
	wp_cache_flush( 'bb_topic' );
	wp_cache_flush( 'bb_forum' );
	wp_cache_flush( 'bb_forums' );
	wp_cache_flush( 'bb_query' );
	wp_cache_flush( 'bb_cache_posts_post_ids' );    
	
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);        
}

function m_approve_post_func($xmlrpc_params)
{
	return xmlrespfalse("Not supported");     
}

function m_ban_user_func($xmlrpc_params)
{
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'user_name'    => Tapatalk_Input::STRING,
		'mode'         => Tapatalk_Input::INT,
		'reason_text'  => Tapatalk_Input::STRING,
	), $xmlrpc_params);		
	
	$user = bb_get_user_by_name($input['user_name']);                    
	if(!$user)
		return xmlrespfalse("User not found");
		
	if(!bb_current_user_can('edit_user', $user->ID) || $user->ID == bb_get_current_user()->ID)
		return xmlrespfalse("Not permitted");
	
	if ( !array_key_exists($role, $user->capabilities) ) {
			$user_obj = new BP_User($user->ID);
			$user_obj->set_role('blocked'); // Only support one role for now
			if ( 'blocked' == $role && 'blocked' != $old_role )
				bb_break_password( $user->ID );
			elseif ( 'blocked' != $role && array_key_exists( 'blocked', $user->capabilities ) )
				bb_fix_password( $user->ID );
	}
		
	if ($input['mode'] == 2) {
		$posts = $bbdb->get_col($bbdb->prepare("SELECT post_id FROM $bbdb->posts WHERE poster_id = %d", $user->ID));
		if(!empty($posts)){
			foreach($posts as $post_id){
				bb_delete_post($post_id, 1);
			}
		}
	}		
				
	$response = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'is_login_mod'  => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval("", 'base64')
	), 'struct');
	
	return new xmlrpcresp($response);        
}


// Moderation Queue

function m_get_moderate_topic_func($xmlrpc_params)
{	
	$result = new xmlrpcval(array(
		'total_topic_num' => new xmlrpcval(0, 'int'),
		'topics'          => new xmlrpcval(array(), 'array'),
	), 'struct');

	return new xmlrpcresp($result);
}

function m_get_delete_topic_func($xmlrpc_params)
{    
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'start_num' => Tapatalk_Input::INT,
		'last_num' => Tapatalk_Input::INT,
	), $xmlrpc_params);    
	
	if(!bb_current_user_can('browse_deleted'))
			return xmlrespfalse("You do not have permission to perform this action");
	
	list($start, $limit, $page) = process_page($input['start_num'], $input['last_num']);
	
	$topic_list = array();    
	
	$total = $bbdb->get_var("
		SELECT COUNT(*)
		FROM $bbdb->topics AS t 
		WHERE t.topic_status <> '0'
	");
	
	$messages = $bbdb->get_results($bbdb->prepare("
		SELECT t.*, p.*, f.*
		FROM $bbdb->topics AS t 
		left join $bbdb->posts AS p ON p.topic_id = t.topic_id
		left join $bbdb->forums AS f ON f.forum_id = t.forum_id
		WHERE t.topic_status <> '0'
		group by t.topic_id
		ORDER BY t.topic_time DESC 
		LIMIT %d,%d", $start, $limit), ARRAY_A);
	
	if(count($messages) == 0)
		return xmlrespfalse("No threads found");

	foreach($messages as $message){                                          
		
		$user = bb_get_user($message['topic_poster']);        
	
		$new_topic = array(
			'forum_id'                => new xmlrpcval($message['forum_id'], 'string'),
			'forum_name'              => new xmlrpcval(basic_clean($message['forum_name']), 'base64'),
			'topic_id'                => new xmlrpcval($message['topic_id'], 'string'),
			'topic_title'             => new xmlrpcval(basic_clean($message['topic_title']), 'base64'),
			//'moderated_by_name'       => new xmlrpcval('', 'base64'), 
			'deleted_by_name'         => new xmlrpcval('', 'base64'), 
			'delete_reason'           => new xmlrpcval('', 'base64'), 
			
			'topic_author_name'       => new xmlrpcval(get_user_display_name($user->ID), 'base64'),
	'topic_author_display_name'       => new xmlrpcval(get_user_display_name($user->ID), 'base64'),
			'deleted_by_icon_url'     => new xmlrpcval('', 'string'),
			'icon_url'                => new xmlrpcval(absolute_url(tt_get_avatar($message['topic_poster'])), 'string'),
			'post_time'               => new xmlrpcval(mobiquo_iso8601_encode(strtotime($message['topic_start_time'])), 'dateTime.iso8601'),
			'short_content'           => new xmlrpcval(process_short_content($message['post_text']), 'base64'),                
		);
	
		$topic_list[] = new xmlrpcval($new_topic, 'struct');
		
	}                

	$result = new xmlrpcval(array(
		'total_topic_num' => new xmlrpcval($total, 'int'),
		'topics'          => new xmlrpcval($topic_list, 'array'),
	), 'struct');

	return new xmlrpcresp($result);
}


function m_get_moderate_post_func($xmlrpc_params)
{	
	$result = new xmlrpcval(array(
		'total_post_num' => new xmlrpcval(0, 'int'),
		'posts'          => new xmlrpcval(array(), 'array')
	), 'struct');

	return new xmlrpcresp($result);
}



function m_get_delete_post_func($xmlrpc_params)
{    
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'start_num' => Tapatalk_Input::INT,
		'last_num' => Tapatalk_Input::INT,
	), $xmlrpc_params);    
	
	if(!bb_current_user_can('browse_deleted'))
			return xmlrespfalse("You do not have permission to perform this action");
	
	list($start, $limit, $page) = process_page($input['start_num'], $input['last_num']);
	
	$topic_list = array();    
	
	$total = $bbdb->get_var("
		SELECT COUNT(*)
		FROM $bbdb->posts AS p
		WHERE p.post_status <> '0'
	");
	
	$messages = $bbdb->get_results($bbdb->prepare("
		SELECT t.*, p.*, f.*
		FROM $bbdb->posts AS p
		left join $bbdb->topics AS t ON p.topic_id = t.topic_id
		left join $bbdb->forums AS f ON f.forum_id = t.forum_id
		WHERE p.post_status <> '0'
		ORDER BY p.post_time DESC 
		LIMIT %d,%d", $start, $limit), ARRAY_A);
	
	if(count($messages) == 0)
		return xmlrespfalse("No posts found");

	foreach($messages as $message){                                          
		
		$user = bb_get_user($message['poster_id']);        
	
		$new_topic = array(
			'forum_id'                => new xmlrpcval($message['forum_id'], 'string'),
			'forum_name'              => new xmlrpcval(basic_clean($message['forum_name']), 'base64'),
			'topic_id'                => new xmlrpcval($message['topic_id'], 'string'),
			'topic_title'             => new xmlrpcval(basic_clean($message['topic_title']), 'base64'),
			'post_id'                => new xmlrpcval($message['post_id'], 'string'),
			'post_title'             => new xmlrpcval('', 'base64'),
			//'moderated_by_name'       => new xmlrpcval('', 'base64'), 
			'deleted_by_name'         => new xmlrpcval('', 'base64'), 
			//'delete_reason'           => new xmlrpcval('', 'base64'), 
			
			'post_author_name'       => new xmlrpcval(get_user_display_name($user->ID), 'base64'),
	'post_author_display_name'       => new xmlrpcval(get_user_display_name($user->ID), 'base64'),
			'deleted_by_icon_url'     => new xmlrpcval('', 'string'),
			'icon_url'                => new xmlrpcval(absolute_url(tt_get_avatar($message['topic_poster'])), 'string'),
			'post_time'               => new xmlrpcval(mobiquo_iso8601_encode(strtotime($message['post_time'])), 'dateTime.iso8601'),
			'short_content'           => new xmlrpcval(process_short_content($message['post_text']), 'base64'),                
		);
	
		$topic_list[] = new xmlrpcval($new_topic, 'struct');
		
	}                

	$result = new xmlrpcval(array(
		'total_post_num' => new xmlrpcval($total, 'int'),
		'posts'          => new xmlrpcval($topic_list, 'array'),
	), 'struct');

	return new xmlrpcresp($result);
}