<?php

defined('IN_MOBIQUO') or exit;


function get_participated_forum_func($xmlrpc_params)
{        
	global $bbdb;
	
	$forums = bb_get_forums();
	
	$user = bb_get_current_user();                   
	if(!$user)
		return xmlrespfalse("User not found");
		
	$participated_forums = $bbdb->get_col($bbdb->prepare("
		select forum_id from $bbdb->posts 
		where poster_id = %d
		group by forum_id
		order by post_time desc", $user->ID));        
	
	$xml_forums = array();
	foreach($participated_forums as $forum_id){
		if(!empty($forums[$forum_id])){
			$forum = $forums[$forum_id];
			$xml_forums[]= new xmlrpcval(array(
				'forum_id'     => new xmlrpcval($forum->forum_id, 'string'),
				'icon_url'     => new xmlrpcval('', 'string'),
				'is_protected'  => new xmlrpcval(false, 'boolean'),
				'new_post'      => new xmlrpcval(false, 'boolean'),
				'forum_name'    => new xmlrpcval(basic_clean($forum->forum_name), 'base64'),
			), 'struct');
		}
	}
	
	$result = new xmlrpcval(array(
		'total_forums_num'=> new xmlrpcval(count($xml_forums), 'boolean'),
		'forums'          => new xmlrpcval($xml_forums, 'array'),
	), 'struct');

	return new xmlrpcresp($result);
}


