<?php

defined('IN_MOBIQUO') or exit;

function get_topic_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'forum_id' => Tapatalk_Input::INT,
		'start_num' => Tapatalk_Input::INT,
		'last_num' => Tapatalk_Input::INT,
		'mode' => Tapatalk_Input::STRING,
	), $xmlrpc_params);    
	
	list($start, $limit, $page) = process_page($input['start_num'], $input['last_num']);
	
	$topic_list = array();	
	
	if(!$forum = bb_get_forum($input['forum_id'])){
		return xmlrespfalse("Forum not found");
	}
	
	
	$old_script_name = $_SERVER['SCRIPT_NAME'];
	$_SERVER['SCRIPT_NAME'] = 'forum.php';
	switch($input['mode']){
		case "TOP":		
		
			$q = array(
				'forum_id' => $input['forum_id'],
				'page' => $page,
				'per_page' => $limit,
				'index_hint' => 'USE INDEX (`forum_time`)',
				'sticky' => 'sticky'
			);
			$query = new BB_Query( 'topic', $q, 'get_latest_topics' );
			$topics = $query->results;
			$forum->topics = count($topics);

			break;            
		case "ANN":
			$topics = array();
			break;
		default:		
			$q = array(
				'forum_id' => $input['forum_id'],
				'page' => $page,
				'per_page' => $limit,
				'index_hint' => 'USE INDEX (`forum_time`)',
				'sticky' => 0
			);
			$query = new BB_Query( 'topic', $q, 'get_latest_topics' );
			$topics = $query->results;
			
			$q = array(
				'forum_id' => $input['forum_id'],
				'page' => $page,
				'per_page' => $limit,
				'index_hint' => 'USE INDEX (`forum_time`)',
				'sticky' => 'sticky'
			);
			$query = new BB_Query( 'topic', $q, 'get_latest_topics' );
			$forum->topics = ($forum->topics) -count($query->results);
			
			break;
	}
	$_SERVER['SCRIPT_NAME'] = $old_script_name;
		
		foreach($topics as $message){
					
			$op = bb_get_first_post($message->topic_id, true);
			
			//$forum = bb_get_forum($topic->forum_id);
			$user = bb_get_user($message->topic_poster);        					

			$new_topic = array(
				'forum_id'          => new xmlrpcval($message->forum_id, 'string'),
				'forum_name'        => new xmlrpcval(basic_clean($forum->forum_name), 'base64'),
				'topic_id'          => new xmlrpcval($message->topic_id, 'string'),
				'topic_title'       => new xmlrpcval(basic_clean($message->topic_title), 'base64'),
				'topic_author_name'  => new xmlrpcval(get_user_display_name($message->topic_poster), 'base64'),
				'last_reply_author_name'  => new xmlrpcval(get_user_display_name($message->topic_last_poster), 'base64'),
		'last_reply_author_display_name'  => new xmlrpcval(get_user_display_name($message->topic_last_poster), 'base64'),
				'icon_url'          => new xmlrpcval(absolute_url(tt_get_avatar($message->topic_poster)), 'string'),
				'last_reply_time'   => new xmlrpcval(mobiquo_iso8601_encode(strtotime($message->topic_time)), 'dateTime.iso8601'),
				'reply_number'      => new xmlrpcval($message->topic_posts - 1, 'int'),
				'view_number'       => new xmlrpcval(0, 'int'),
				'new_post'          => new xmlrpcval(false, 'boolean'),
				'short_content'     => new xmlrpcval(process_short_content($op->post_text), 'base64'),
				'can_subscribe'     => new xmlrpcval(bb_current_user_can('edit_favorites'), 'boolean'),             
				'is_subscribed'     => new xmlrpcval(is_user_favorite(bb_get_current_user()->ID, $message->topic_id), 'boolean'),
				'is_closed'         => new xmlrpcval(!$message->topic_open, 'boolean'),
						
	
				'can_delete'        => new xmlrpcval(bb_current_user_can('delete_topic', $message->topic_id), 'boolean'),
				'can_close'         => new xmlrpcval(bb_current_user_can('close_topic', $message->topic_id), 'boolean'),
				'can_approve'       => new xmlrpcval(false, 'boolean'),
				'can_stick'         => new xmlrpcval(bb_current_user_can('stick_topic', $message->topic_id), 'boolean'),
				'can_move'          => new xmlrpcval(bb_current_user_can('move_topic', $message->topic_id), 'boolean'),
				'can_ban'           => new xmlrpcval((bb_current_user_can('edit_user', $message->topic_poster) && ($message->topic_poster != bb_get_current_user()->ID)) && bb_current_user_can('moderate'), 'boolean'),
				'can_rename'        => new xmlrpcval(bb_current_user_can('edit_topic', $message->topic_id), 'boolean'), 
				'is_ban'            => new xmlrpcval(array_key_exists('blocked', $user->capabilities), 'boolean'),
				'is_sticky'         => new xmlrpcval($message->topic_sticky, 'boolean'),
				'is_approved'       => new xmlrpcval(true, 'boolean'),
				'is_deleted'        => new xmlrpcval($message->topic_status != 0, 'boolean')
					
			);
		
				
			if($_SERVER['HTTP_MOBIQUO_ID'] == 10)
			{
				$avatars = $bbdb->get_results($bbdb->prepare("
					select poster_id 
					from $bbdb->posts
					where topic_id = %d
					group by poster_id
					order by count(poster_id) desc
					limit 10
				", $message->topic_id));
				$icon_urls_list = array();   
				foreach($avatars as $avatar_user_id){
					if(empty($avatar_user_id))
						continue;
					$icon_urls_list []= new xmlrpcval($avatar_user_id, "string");
				}      
				$new_topic['participated_uids'] = new xmlrpcval($icon_urls_list, 'array');       
			}
			
			$topic_list[] = new xmlrpcval($new_topic, 'struct');
			
		}				


	$result = new xmlrpcval(array(
		'total_topic_num' => new xmlrpcval($forum->topics, 'int'),
		'forum_id'        => new xmlrpcval($forum->forum_id, 'string'),
		'forum_name'      => new xmlrpcval(basic_clean($forum->forum_name), 'base64'),
		'can_post'        => new xmlrpcval(bb_current_user_can('write_topic', $forum->forum_id), 'boolean'),
		'require_prefix'  => new xmlrpcval(false, 'boolean'),
		'prefixes'        => new xmlrpcval(array(), 'array'),
		'topics'          => new xmlrpcval($topic_list, 'array'),
		'can_upload'      => new xmlrpcval(false, 'boolean'),
		'unread_sticky_count' => new xmlrpcval(0, 'int'),
		'unread_announce_count' => new xmlrpcval(0, 'int'),
	), 'struct');

	return new xmlrpcresp($result);
}
