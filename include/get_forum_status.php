<?php

defined('IN_MOBIQUO') or exit;


function get_forum_status_func($xmlrpc_params)
{    
	$input = Tapatalk_Input::filterXmlInput(array(
		'forum_ids' => Tapatalk_Input::RAW
	), $xmlrpc_params);      
	
	$forums = bb_get_forums();
	
	$xml_forums = array();
	
	foreach($input['forum_ids'] as $forum_id){
		if(!empty($forums[$forum_id])){
			$forum = $forums[$forum_id];
			$xml_forums[]= new xmlrpcval(array(
				'forum_id'     => new xmlrpcval($forum->forum_id, 'string'),
				'icon_url'     => new xmlrpcval('', 'string'),
				'is_protected'  => new xmlrpcval(false, 'boolean'),
				'new_post'      => new xmlrpcval(false, 'boolean'),
				'forum_name'    => new xmlrpcval(basic_clean($forum->forum_name), 'base64'),
			), 'struct');
		}
	}
	
	$xml_nodes = new xmlrpcval($xml_forums, 'array');
	
	return new xmlrpcresp($xml_nodes);
}


