<?php

defined('IN_MOBIQUO') or exit;

function get_board_stat_func()
{
	global $bbdb;
	
	$topics  = $bbdb->get_var("SELECT COUNT(*) FROM $bbdb->topics WHERE topic_status = 0");
	$posts   = $bbdb->get_var("SELECT COUNT(*) FROM $bbdb->posts WHERE post_status = 0");

	$users   = $bbdb->get_var("SELECT COUNT(*) FROM $bbdb->users");
	
	$board_stat = array(
		'total_threads' => new xmlrpcval($topics, 'int'),
		'total_posts'   => new xmlrpcval($posts, 'int'),
		'total_members' => new xmlrpcval($users, 'int'),
		'guest_online'  => new xmlrpcval(0, 'int'),
		'total_online'  => new xmlrpcval(0, 'int')
	);
	
	$response = new xmlrpcval($board_stat, 'struct');
	
	return new xmlrpcresp($response);
}
