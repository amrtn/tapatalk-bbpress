<?php

defined('IN_MOBIQUO') or exit;

function reply_post_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
			'forum_id' => Tapatalk_Input::INT,
			'topic_id' => Tapatalk_Input::INT,
			'subject' => Tapatalk_Input::STRING,
			'text_body' => Tapatalk_Input::STRING,
			'attachment_id_array' => Tapatalk_Input::RAW,
			'group_id' => Tapatalk_Input::INT,
			'return_html' => Tapatalk_Input::INT,
	), $xmlrpc_params);
		
	if(!$topic = get_topic($input['topic_id']))
		return xmlrespfalse("Invalid topic specified");
		
	if(!bb_current_user_can('write_topic') || !bb_current_user_can('write_post'))
		return xmlrespfalse("You do not have permission to post");
		
	if(empty($input['text_body']))
		return xmlrespfalse("You must enter a message");
	
	if(!$post = bb_insert_post(array(
		'topic_id' => $topic->topic_id,
		'post_text' => $input['text_body']
	)))
		return xmlrespfalse("The post could not be created");
	$post = bb_get_post($post);  
		

	$result = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval('', 'base64'),
		'topic_id'      => new xmlrpcval($post->topic_id, 'string'),
		'post_author_name' => new xmlrpcval(get_user_display_name($post->poster_id), 'base64'),
		'icon_url'         => new xmlrpcval(absolute_url(tt_get_avatar($post->poster_id)), 'string'),
		'state'         => new xmlrpcval($post->post_status == 0 ? 0 : 1, 'int'),
		'post_content'  => new xmlrpcval(process_post($post->post_text, $input['return_html']), 'base64'),
		'can_edit'      => new xmlrpcval(bb_current_user_can('edit_post', $post->post_id), 'boolean'),
		'can_delete'    => new xmlrpcval(bb_current_user_can('delete_post', $post->post_id), 'boolean'),
		'post_time'     => new xmlrpcval(mobiquo_iso8601_encode(strtotime($post->post_time)), 'dateTime.iso8601'),
		//'attachments'   => new xmlrpcval($attachment_list, 'array'),
	), 'struct');
		
	return new xmlrpcresp($result);
}

		