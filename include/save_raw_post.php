<?php

defined('IN_MOBIQUO') or exit;

function save_raw_post_func($xmlrpc_params)
{    
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
			'post_id' => Tapatalk_Input::INT,
			'post_title' => Tapatalk_Input::STRING,
			'post_content' => Tapatalk_Input::STRING,
			'return_html' => Tapatalk_Input::INT,
	), $xmlrpc_params);
		
		
	if(!$post = bb_get_post($input['post_id']))
		return xmlrespfalse("Post not found");    
	
	if(!$topic = get_topic($post->topic_id))
		return xmlrespfalse("Topic not found");
	
	if(!bb_current_user_can('edit_post', $post->post_id))
		return xmlrespfalse("You do not have permission to edit this post");		

	if(empty($input['post_content']))
		return xmlrespfalse("You must enter a message");
	
	$title = false;
	
	if(bb_is_first($post->post_id) && bb_current_user_can('edit_topic', $topic->topic_id)){        
		if(empty($input['post_title']))
			return xmlrespfalse("You must enter a subject");
		
		if(!bb_insert_topic(array(
			'topic_title' => $input['post_title'],
			'topic_id' => $topic->topic_id
		)))
			return xmlrespfalse("The topic could not be saved");
				
		$title = true;
		$topic = get_topic($post->topic_id);
	}
	
	
	if(!bb_insert_post(array(
		'post_id' => $post->post_id,
		'post_text' => $input['post_content']
	)))
		return xmlrespfalse("The post could not be saved");

	
	$post = bb_get_post($post->post_id);
					
	$result = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval('', 'base64'),
		'state'         => new xmlrpcval($post->post_status == 0, 'int'),
		'post_title'    => new xmlrpcval($title ? $topic->topic_title : "", 'base64'),
		'post_content'  => new xmlrpcval(process_post($post->post_text, $input['return_html']), 'base64'),
	), 'struct');
		
	return new xmlrpcresp($result);
}
