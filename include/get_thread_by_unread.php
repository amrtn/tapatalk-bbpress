<?php

defined('IN_MOBIQUO') or exit;

require_once "include/get_thread.php";

function get_thread_by_unread_func($xmlrpc_params)
{	
	global $bbdb;
	
	$input = Tapatalk_Input::filterXmlInput(array(
		'topic_id'    => Tapatalk_Input::INT,
		'posts_per_request'   => Tapatalk_Input::INT,
		'return_html' => Tapatalk_Input::INT
	), $xmlrpc_params);
	
	if(!$input['posts_per_request'])
		$input['posts_per_request'] = 20;
	
	return get_thread_func(new xmlrpcval(array(
		new xmlrpcval($input['topic_id'], "string"),
		new xmlrpcval(0, 'int'),
		new xmlrpcval($input['posts_per_request'], 'int'),
		new xmlrpcval(!!$input['return_html'], 'boolean'),
	), 'array'));
	
}
