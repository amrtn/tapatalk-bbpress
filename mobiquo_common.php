<?php

defined('IN_MOBIQUO') or exit;



function get_error($error_key, $params = array())
{
	$error_message = (string)new XenForo_Phrase($error_key, $params);
	
	$r = new xmlrpcresp(
			new xmlrpcval(array(
				'result'        => new xmlrpcval(false, 'boolean'),
				'result_text'   => new xmlrpcval($error_message, 'base64'),
			),'struct')
	);
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".$r->serialize('UTF-8');
	exit;
}


function errors(array $errors){
	if(empty($errors))
		return;
		
	$error = "";
	foreach($errors as $err){
		$error .= "$err\r\n";
	}
	error($error);
}

function tt_get_avatar($user_id){
	$size = 144; $default = ''; $alt = false;
		
	if ( !bb_get_option('avatars_show') )
		return '';

	if ( $email = bb_get_user_email($user_id) ) {
	} else {
		$email = $user_id;
	}

	if ( !$email )
		$email = '';

	if ( empty($default) )
		$default = bb_get_option('avatars_default');

	if ( is_ssl() )
		$host = 'https://secure.gravatar.com';
	else
		$host = 'http://www.gravatar.com';

	switch ($default) {
		case 'logo':
			$default = '';
			break;
		case 'blank':
			$default = bb_get_uri( 'bb-admin/images/blank.gif', null, BB_URI_CONTEXT_IMG_SRC );
			break;
		case 'monsterid':
		case 'wavatar':
		case 'identicon':
		case 'retro':
			break;
		case 'default':
		default:
			$default = $host . '/avatar/ad516503a11cd5ca435acc9bb6523536?s=' . $size;
			// ad516503a11cd5ca435acc9bb6523536 == md5('unknown@gravatar.com')
			break;
	}

	$src = $host . '/avatar/';
	$class .= 'avatar avatar-' . $size;

	if ( !empty($email) ) {
		$src .= md5( strtolower( $email ) );
	} else {
		$src .= 'd41d8cd98f00b204e9800998ecf8427e';
		// d41d8cd98f00b204e9800998ecf8427e == md5('')
	}

	$src .= '?s=' . $size;
	$src .= '&amp;d=' . urlencode( $default );

	$rating = bb_get_option('avatars_rating');
	if ( !empty( $rating ) )
		$src .= '&amp;r=' . $rating;

	return $src;
}


function get_method_name()
{
	$ver = phpversion();
	if ($ver[0] >= 5) {
		$data = file_get_contents('php://input');
	} else {
		$data = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
	}
	$parsers = php_xmlrpc_decode_xml($data);
	if(is_object($parsers))
		return trim($parsers->methodname);
	else
	{
		if(!empty($_POST['method_name']))
			return $_POST['method_name'];
		else
			return null;
	}
}

function get_mobiquo_config() 
{
	$config_file = './config/config.txt';
	file_exists($config_file) or exit('config.txt does not exists');
	
	if(function_exists('file_get_contents')){
		$tmp = file_get_contents($config_file);
	}else{
		$handle = fopen($config_file, 'rb');
		$tmp = fread($handle, filesize($config_file));
		fclose($handle);
	}
	
	// remove comments by /*xxxx*/ or //xxxx
	$tmp = preg_replace('/\/\*.*?\*\/|\/\/.*?(\n)/si','$1',$tmp);
	$tmpData = preg_split("/\s*\n/", $tmp, -1, PREG_SPLIT_NO_EMPTY);
	
	$mobiquo_config = array();
	foreach ($tmpData as $d){
		list($key, $value) = preg_split("/=/", $d, 2); // value string may also have '='
		$key = trim($key);
		$value = trim($value);
		if ($key == 'hide_forum_id')
		{
			$value = preg_split('/\s*,\s*/', $value, -1, PREG_SPLIT_NO_EMPTY);
			count($value) and $mobiquo_config[$key] = $value;
		}
		else
		{
			strlen($value) and $mobiquo_config[$key] = $value;
		}
	}
	
	return $mobiquo_config;
}

function xmlresptrue()
{
	$result = new xmlrpcval(array(
		'result'        => new xmlrpcval(true, 'boolean'),
		'result_text'   => new xmlrpcval('', 'base64')
	), 'struct');
	
	return new xmlrpcresp($result);
}

function xmlrespfalse($error_message)
{
	$result = new xmlrpcval(array(
		'result'        => new xmlrpcval(false, 'boolean'),
		'result_text'   => new xmlrpcval(strip_tags($error_message), 'base64')
	), 'struct');
	
	return new xmlrpcresp($result);
}

function tt_error($error="", $title="")
{
	if(!empty($title))
		$error = "{$title} :: {$error}";
	$error = strip_tags($error);    
	
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".(xmlresperror($error)->serialize('UTF-8'));
	exit;
}

function xmlresperror($error_message)
{
	$result = new xmlrpcval(array(
		'result'        => new xmlrpcval(false, 'boolean'),
		'result_text'   => new xmlrpcval($error_message, 'base64')
	), 'struct');

	return new xmlrpcresp($result/*, 98, $error_message*/);
}


function tt_no_permission(){
	return xmlrespfalse('You do not have permission to view this');
}

function absolute_url($url){
	if(empty($url))
		return "";
	if(substr($url, 0, 7) != 'http://'){
		$url = bb_get_uri( $url );
	}
	return  str_replace("mobiquo/", "", $url);
}



function get_forum_icon_url($fid)
{
	$logo_url = '';
	if (file_exists("./forum_icons/$fid.png"))
	{
		$logo_url = FORUM_ROOT."mobiquo/forum_icons/$fid.png";
	}
	else if (file_exists("./forum_icons/$fid.jpg"))
	{
		$logo_url = FORUM_ROOT."mobiquo/forum_icons/$fid.jpg";
	}
	else if (file_exists("./forum_icons/default.png"))
	{
		$logo_url = FORUM_ROOT."mobiquo/forum_icons/default.png";
	}
	
	return $logo_url;
}

function mobiquo_iso8601_encode($timet)
{
	$offset = wp_timezone_override_offset();
	
	$t = gmdate("Ymd\TH:i:s", $timet + $offset * 3600);
	$t .= sprintf("%+03d:%02d", intval($offset), abs($offset - intval($offset)) * 60); 
		
	return $t;
}

function cutstr($string, $length)
{
	if(strlen($string) <= $length) {
		return $string;
	}

	$string = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;'), array('&', '"', '<', '>'), $string);

	$strcut = '';

	$n = $tn = $noc = 0;
	while($n < strlen($string)) {

		$t = ord($string[$n]);
		if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
			$tn = 1; $n++; $noc++;
		} elseif(194 <= $t && $t <= 223) {
			$tn = 2; $n += 2; $noc += 2;
		} elseif(224 <= $t && $t <= 239) {
			$tn = 3; $n += 3; $noc += 2;
		} elseif(240 <= $t && $t <= 247) {
			$tn = 4; $n += 4; $noc += 2;
		} elseif(248 <= $t && $t <= 251) {
			$tn = 5; $n += 5; $noc += 2;
		} elseif($t == 252 || $t == 253) {
			$tn = 6; $n += 6; $noc += 2;
		} else {
			$n++;
		}

		if($noc >= $length) {
			break;
		}

	}
	if($noc > $length) {
		$n -= $tn;
	}

	$strcut = substr($string, 0, $n);
	
	return $strcut;
}

function process_short_content($post_text, $length = 200)
{	
	$post_text = strip_tags($post_text);
	
	//$post_text = $parser->parse_badwords($post_text);
	$post_text = preg_replace('/\[url.*?\].*?\[\/url.*?\]/', '[url]', $post_text);
	$post_text = preg_replace('/\[img.*?\].*?\[\/img.*?\]/', '[img]', $post_text);
	$post_text = preg_replace('/[\n\r\t]+/', ' ', $post_text);
	
	// strip bbcode
	//$post_text = preg_replace('/\[[^]]*]([^[]*)\[\/[^]]*]/s', '', $post_text);
	//$post_text = preg_replace('/\[[^]]*=.*?]([^[]*)\[\/[^]]*]/s', '', $post_text);
	$post_text = preg_replace('/\[\/?.*?]/s', '', $post_text);

	
	$post_text = html_entity_decode($post_text, ENT_QUOTES, 'UTF-8');
	$post_text = function_exists('mb_substr') ? mb_substr($post_text, 0, $length) : substr($post_text, 0, $length);
	return $post_text;
}

function process_post($post, $returnHtml = false)
{
	if($returnHtml){
		$post = strip_tags($post, '<strong><a><blockquote><em><img><code>');
		$post = str_replace("&", '&amp;', $post);
		$post = str_ireplace("<strong>", '<b>', $post);
		$post = str_ireplace("</strong>", '</b>', $post);
		$post = str_ireplace("<blockquote>", '[quote]', $post);
		$post = str_ireplace("</blockquote>", '[/quote]', $post);
		$post = str_ireplace("<em>", '<i>', $post);
		$post = str_ireplace("</em>", '</i>', $post);
		$post = preg_replace('#<a[^>]+href=[\'"](.*?)[\'"].*?>(.*?)</a>#is', '[url="$1"]$2[/url]', $post);
		$post = preg_replace('#<img[^>]+src=[\'"](.*?)[\'"].*?/>#is', '[img]$1[/img]', $post);
		$post = preg_replace('#`(.*?)`#is', '[quote]$1[/quote]', $post);
		$post = preg_replace('#<code>(.*?)</code>#is', '[quote]$1[/quote]', $post);
		$post = str_replace("\r", '', $post);
		$post = str_replace("\n", '<br />', $post);
		//$post = str_replace('[hr]', '<br />____________________________________<br />', $post);
		//$post = str_replace("<", '&lt;', $post);
		//$post = str_replace(">", '&gt;', $post);
		// strip remaining bbcode
	} else {
		$post = str_ireplace("<blockquote>", '[quote]', $post);
		$post = str_ireplace("</blockquote>", '[/quote]', $post);
		$post = strip_tags($post);
		$post = html_entity_decode($post, ENT_QUOTES, 'UTF-8');
		$post = str_replace('[hr]', "\n____________________________________\n", $post);
	}
	
	$post = preg_replace('/\[quote="(.*?)".*?\]/si', '$1 wrote:[quote]', $post);
	$post = preg_replace('/\[attachment.*?\[\/attachment\]/s', '', $post);
	$post = preg_replace('/\[(\/?(?:img|url|quote))/', '<$1', $post);
	$post = preg_replace('/\[\/?.*?\]/si', '', $post);
	$post = preg_replace('/<(\/?(?:img|url|quote))/', '[$1', $post);
	
	$post = trim($post);
	// remove link on img
	$post = preg_replace('/\[url=[^\]]*?\]\s*(\[img\].*?\[\/img\])\s*\[\/url\]/si', '$1', $post);
	//$post = preg_replace('/([^"\'\]](https?\:\/\/|www\.)[\w-&#;\?\.\/\=]+)/is', '[url]$1[/url]', $post);
	$post = preg_replace('/((https?:\/\/|www\.)[\w-&#;\?\.\/\=]+)/is', '[url]$1[/url]', $post);
//	$post = preg_replace('/\[url=[^\]]*?\]\s*(\[img\].*?\[\/img\])\s*\[\/url\]/si', '[img]$1[/img]', $post);
	return $post;
}

function process_page($start_num, $end)
{
	$start = intval($start_num);
	$end = intval($end);
	$start = empty($start) ? 0 : max($start, 0);
	$end = (empty($end) || $end < $start) ? ($start + 19) : max($end, $start);
	if ($end - $start >= 50) {
		$end = $start + 49;
	}
	$limit = $end - $start + 1;
	$page = intval($start/$limit) + 1;
	
	return array($start, $limit, $page);
}

function basic_clean($str)
{
	$str = strip_tags($str);
	$str = trim($str);
	return html_entity_decode($str, ENT_QUOTES, 'UTF-8');
}

function get_extension($file)
{
	return strtolower(substr(strrchr($file, "."), 1));
}


